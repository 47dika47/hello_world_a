import 'package:flutter/material.dart';
import 'products.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;

  ProductManager({this.startingProduct = "Food Tester"});

  @override
  _ProductManagerState createState(){
    print('[ProductManagerState] createState()');
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  @override
  void iniState() {
    print('[ProductManagerState] iniState()');
    _products.add(widget.startingProduct);
    super.initState();
  }

  @override
  void didUpdateWigdet(ProductManager oldWidget) {
    print('[ProductManager State] didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManagerState] bulid ()');
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10.0),
          child: RaisedButton(
            color: Theme.of(context).primaryColor,
            child: Text('Tambah Produk'),
            onPressed: () {
              setState(() {
                _products.add('Food Vaganza');
                print(_products);
              });
            },
          ),
        ),
      Products(_products),
      ],
    );
  }
}
